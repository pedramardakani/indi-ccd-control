/*************************************************************************
Set temperature

This file is part of the INDI CCD CONTROL program which will let the users
set up CCD configuration directly from the command-line, without the need
to have a knowledge of cpp language.

INDI already works with GUI programs, and has its libraries and
clients. However, this command-line client helps programmers write scripts
to automate the process without writing in C++.

Copyright (C) 2022 Pedram Ashofteh Ardakani <pedramardakani@pm.me>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 3 or later as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.LIB.  If not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*************************************************************************/

#pragma once

#include "baseclient.h"


/* The property name we are looking for: */
char propname[] = "CCD_TEMPERATURE";

/* Condition to stop the program automatically. */
std::mutex mtx;
bool processed = false;
std::condition_variable cv;

/* Current CCD_TEMPERATURE_VALUE */
double ccdtemperature;



class MyClient : public INDI::BaseClient
{
public:
  MyClient();
  ~MyClient() = default;

  void settemperature();
  void printNumberVector(INumberVectorProperty *);

protected:
  void newBLOB(IBLOB *bp) override {}
  void serverConnected() override {}
  void serverDisconnected(int) override {}
  void newDevice(INDI::BaseDevice *) override;
  void newText(ITextVectorProperty *) override {}
  void newLight(ILightVectorProperty *) override {}
  void removeDevice(INDI::BaseDevice *) override {}
  void removeProperty(INDI::Property *) override {}
  void newNumber(INumberVectorProperty *) override;
  void newSwitch(ISwitchVectorProperty *) override {}
  void newProperty(INDI::Property *) override;
  void newMessage(INDI::BaseDevice *dp, int messageID) override;

private:
    INDI::BaseDevice *ccd_simulator;
};
