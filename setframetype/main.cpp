/*************************************************************************
Set frame type

This file is part of the INDI CCD CONTROL program which will let the users
set up CCD configuration directly from the command-line, without the need
to have a knowledge of cpp language.

INDI already works with GUI programs, and has its libraries and
clients. However, this command-line client helps programmers write scripts
to automate the process without writing in C++.

Copyright (C) 2022 Pedram Ashofteh Ardakani <pedramardakani@pm.me>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 3 or later as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.LIB.  If not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*************************************************************************/

#pragma once

#include <mutex>
#include <memory>
#include <time.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <condition_variable>

#include "ui.h"
#include "main.h"

#include "indibasetypes.h"
#include "indibase/basedevice.h"

/* INDI Program */
static std::unique_ptr<MyClient> camera_client(new MyClient());

/* Argument Parser */
struct arguments arguments;
static struct argp argp = {options, parse_opt, args_doc, doc};

/* Condition to stop the program automatically */
std::mutex mtx;
bool processed = false;
std::condition_variable cv;

int
main(int argc, char **argv)
{
  /* Default values. */
  arguments.port = 7624;
  arguments.verbose = 0;
  arguments.ccdframetype = "LIGHT";
  arguments.ipaddress = "localhost";
  arguments.device_name = "Simple CCD";

  /* Parse every option seen by parse_opt. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  if (arguments.verbose)
    {
      IDLog("CCDNAME: %s\nIP ADDRESS: %s\nPORT: %d\nCCD FRAME TYPE: %s\n",
          arguments.device_name,
          arguments.ipaddress,
          arguments.port,
          arguments.ccdframetype);
    }

  camera_client->setServer(arguments.ipaddress, arguments.port);
  camera_client->watchDevice(arguments.device_name);

  /* This is the cleanest way of terminating the program I found so
     far. Just return with a non-zero value. */
  if (camera_client->connectServer() != 1)
  {
    IDLog("%s: socket error, abort ...\n", __func__);
    return 1;
  }

  /* Check:
     IDLog(">>> Starting worker ...\n"); */

  // wait for completion signal
  {
    std::unique_lock<std::mutex> lk{mtx};
    cv.wait(lk, [&] { return processed; });
  }

  return 0;
}

MyClient::MyClient() { ccd_simulator = nullptr; }





void MyClient::setframetype()
{
  ISwitchVectorProperty *svp = nullptr;
  svp = ccd_simulator->getSwitch(propname);

  if (svp == nullptr)
    {
      IDLog(">>> %s: '%s' in '%s' not found\n", __func__, propname,
            arguments.device_name);
      return;
    }

  for(int i=svp->nsp; i--;)
    {
      /* Check:
      IDLog("%s: name %s type %s state %d\n",
            __func__, svp->name, svp->sp[i].name, svp->sp[i].s);
      IDLog("%s: comparing '%s' to '%s'\n",
      __func__, arguments.ccdframetype, svp->sp[i].name); */

      if( strcmp(arguments.ccdframetype, svp->sp[i].name)==0 )
        { svp->sp[i].s = ISS_ON; }
      else
        { svp->sp[i].s = ISS_OFF; }
    }

  sendNewSwitch(svp);
}





void MyClient::printSwitchVector(ISwitchVectorProperty *svp)
{
  if (svp == nullptr)
  {
    IDLog("%s: [ERROR] device %s prop %s not found\n",
          __func__, arguments.device_name, svp->name);
    return;
  }

  IDLog("%s: %s Current values:\n", __func__, svp->name);
  for (int i = svp->nsp; i--;)
    IDLog("    %s: %d\n", svp->sp[i].name, svp->sp[i].s);
}

void MyClient::newDevice(INDI::BaseDevice *dp)
{
  // Check if we have got the target device
  if (arguments.verbose) IDLog("%s: '%s'\n", __func__, dp->getDeviceName());
  if (strcmp(dp->getDeviceName(), arguments.device_name) != 0) return;

  // Got the device
  ccd_simulator = dp;
}

void MyClient::newProperty(INDI::Property *property)
{
  const char *property_name = property->getName();
  const char *device_name = property->getDeviceName();

  if( strcmp(device_name, arguments.device_name)!=0 ) return;

  if( arguments.verbose )
    {
      IDLog("%s: '%s' in device '%s'\n",
            __func__, property_name, device_name);
    }

  if( strcmp(property_name, propname)==0 ) { setframetype(); }
  if( strcmp(property_name, "CONNECTION")==0 )
  {
    connectDevice(arguments.device_name);
    return;
  }
}

void MyClient::newMessage(INDI::BaseDevice *dp, int messageID)
{
  if( strcmp(dp->getDeviceName(), arguments.device_name )!=0 ) return;
  IDLog( "Recveing message from Server:\n\n########################\n%s"
         "\n########################\n\n",
         dp->messageQueue(messageID).c_str() );
}

void MyClient::newSwitch(ISwitchVectorProperty *svp)
{
  // Check exposure time
  if( strcmp(svp->name, propname)==0 )
    {
      for (uint8_t i = svp->nsp; i--;)
        { IDLog("%s, %d\n", svp->sp[i].name, svp->sp[i].s); }
    }

  for(uint8_t i=svp->nsp; i--;)
    if( strcmp(arguments.ccdframetype, svp->sp[i].name)==0)
      if(svp->sp[i].s == ISS_ON)
        {
          // Send finish signal
          std::lock_guard<std::mutex> lk{mtx};
          processed = true;
          cv.notify_one();
          break;
        }
}
