# INDI CCD CONTROL

add_executable(setframetype main.cpp)

target_link_libraries(setframetype indiclient ${ZLIB_LIBRARY} ${NOVA_LIBRARIES})
IF (UNIX AND NOT APPLE)
    target_link_libraries(setframetype -lpthread)
ENDIF ()
