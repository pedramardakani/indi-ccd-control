/**************************************************************************
INDI CCD CONTROL

This file is part of the INDI CCD CONTROL program which will let the users
set up CCD configuration directly from the command-line, without the need
to have a knowledge of cpp language.

INDI already works with GUI programs, and has its libraries and
clients. However, this command-line client helps programmers write scripts
to automate the process without writing in C++.

Copyright (C) 2022 Pedram Ashofteh Ardakani <pedramardakani@pm.me>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 3 or later as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.LIB.  If not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
**************************************************************************/

#pragma once

#include <stdlib.h>
#include <argp.h>

const char *argp_program_version = "INOLA CCD CONTROL 0.0.0";
const char *argp_program_bug_address = "<pedramardakani@pm.me>";

/* Program documentation. */
static char doc[] = "Command-line client to control the CCD using "
  "INDI libraries.";

/* A description of the non-option arguments we accept, i.e. mandatory. */
static char args_doc[] = "EXPOSURETIME";

/* The options we understand. */
static struct argp_option options[] = {
  {"verbose", 'v', 0, 0, "Produce output"},
  {"output", 'o', "FILENAME", 0, "Output file name"},
  {"port", 'p', "PORT_NUMBER", 0, "Listen to this port"},
  {"ip", 'i', "IP_ADDRESS", 0, "Listen to this ip address"},
  {"device", 'd', "DEVICE_NAME", 0, "Watch for a specific device"},
  {0}};

/* Used by main to communicate with parse_opt. */
struct arguments
{
  int verbose, port;
  float exposuretime;
  const char *device_name, *ipaddress, *output_name;
};

/* Parse a single option. */
static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we know is a pointer to
     our arguments structure. */
  struct arguments *arguments =
    static_cast<struct arguments *>(state->input);

  switch (key)
    {
    case 'v': arguments->verbose = 1;       break;
    case 'i': arguments->ipaddress = arg;   break;
    case 'p': arguments->port = atoi(arg);  break;
    case 'd': arguments->device_name = arg; break;
    case 'o': arguments->output_name = arg; break;
    case ARGP_KEY_ARG:
      {
        switch (state->arg_num)
          {
          case 0: arguments->exposuretime = atof(arg); break;
          default:
            /* Too many arguments. */
            printf("Too many arguments (%d) ...\n", state->arg_num);
            argp_usage(state);
            break;
          }
      }
      break;
    case ARGP_KEY_END:
      if (state->arg_num != 1)
        {
          printf("Not enough arguments (%d) ...\n", state->arg_num);
          argp_usage(state);
        }
      break;
    default: return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
