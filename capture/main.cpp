/*************************************************************************
INDI CCD CONTROL

This file is part of the INDI CCD CONTROL program which will let the users
set up CCD configuration directly from the command-line, without the need
to have a knowledge of cpp language.

INDI already works with GUI programs, and has its libraries and
clients. However, this command-line client helps programmers write scripts
to automate the process without writing in C++.

Copyright (C) 2022 Pedram Ashofteh Ardakani <pedramardakani@pm.me>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 3 or later as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.LIB.  If not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*************************************************************************/

#include "ui.h"
#include "main.h"

#include "indibasetypes.h"
#include "indibase/basedevice.h"

#include <mutex>
#include <argp.h>
#include <memory>
#include <time.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <condition_variable>

/* INDI Program */
static std::unique_ptr<MyClient> camera_client(new MyClient());

/* Argument Parser */
struct arguments arguments;
static struct argp argp = {options, parse_opt, args_doc, doc};

/* Condition to stop the program automatically */
std::mutex mtx;
bool processed = false;
std::condition_variable cv;

int
main(int argc, char **argv)
{
  /* Default values. */
  arguments.port = 7624;
  arguments.verbose = 0;
  arguments.ipaddress = "localhost";
  arguments.device_name = "Simple CCD";
  arguments.output_name = "output.fits";

  /* Parse every option seen by parse_opt. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  if (arguments.verbose)
    {
      IDLog("CCDNAME: %s\nIP ADDRESS: %s\nPORT: %d\nEXPOSURETIME: %f\n",
          arguments.device_name,
          arguments.ipaddress,
          arguments.port,
          arguments.exposuretime);
    }

  camera_client->setServer(arguments.ipaddress, arguments.port);
  camera_client->watchDevice(arguments.device_name);

  /* This is the cleanest way of terminating the program I found so
     far. Just return with a non-zero value. */
  if (camera_client->connectServer() != 1)
  {
    IDLog("%s: socket error, abort ...\n", __func__);
    return 1;
  }

  camera_client->setBLOBMode(B_ALSO, arguments.device_name, nullptr);

  /* Check:
     IDLog(">>> Starting worker ...\n"); */

  // wait for completion signal
  {
    std::unique_lock<std::mutex> lk{mtx};
    cv.wait(lk, [&] { return processed; });

  }

  return 0;
}

MyClient::MyClient()
{
  ccd_simulator = nullptr;
}

void MyClient::capture()
{
  char prop_name[] = "CCD_EXPOSURE";
  INumberVectorProperty *ccd_exposure = nullptr;
  ccd_exposure = ccd_simulator->getNumber(prop_name);

  if (ccd_exposure == nullptr)
    {
      IDLog(">>> %s: '%s' in '%s' not found\n", __func__, prop_name,
            arguments.device_name);
      return;
    }

  ccd_exposure->np[0].value = arguments.exposuretime;

  /* Check:
     IDLog("%s: sending new number ...\n", __func__); */
  sendNewNumber(ccd_exposure);
}





void MyClient::getInfo(INDI::Property *p)
{
  const char *name = p->getName();

  switch (p->getType())
  {
  case INDI_NUMBER:
    /* Check:
      if(arguments.verbose) printNumberVector( p->getNumber() ); */
    if( strcmp(name, "CCD_EXPOSURE")==0 ) { capture(); }
    break;

    /* Check:
  case INDI_BLOB: printBLOBVector(p->getBLOB());       break;
  case INDI_TEXT: printTextVector(p->getText());       break;
  case INDI_SWITCH: printSwitchVector(p->getSwitch()); break; */
  default:                                             break;
  }
}





void MyClient::printNumberVector(INumberVectorProperty *nvp)
{
  if (nvp == nullptr)
  {
    IDLog("%s: [ERROR] device %s prop %s not found\n",
          __func__, arguments.device_name, nvp->name);
    return;
  }

  IDLog("%s: %s Current values:\n",
        __func__, nvp->name);
  for (int i = nvp->nnp; i--;)
  {
    IDLog("    %s: %f\n",
          nvp->np[i].name,
          nvp->np[i].value);
  }
}

void MyClient::printSwitchVector(ISwitchVectorProperty *svp)
{
  if (svp == nullptr)
  {
    IDLog("%s: [ERROR] device %s prop %s not found\n",
          __func__, arguments.device_name, svp->name);
    return;
  }

  IDLog("%s: %s Current values:\n",
        __func__, svp->name);
  for (int i = svp->nsp; i--;)
  {
    IDLog("    %s: %d\n",
          svp->sp[i].name,
          svp->sp[i].s);
  }
}

void MyClient::printTextVector(ITextVectorProperty *tvp)
{
  if (tvp == nullptr)
  {
    IDLog("%s: [ERROR] device %s prop %s not found\n",
          __func__, arguments.device_name, tvp->name);
    return;
  }

  IDLog("%s: %s Current values:\n", __func__, tvp->name);
  for (int i = tvp->ntp; i--;)
  {
    IDLog("    %s: %s\n",
          tvp->tp[i].name,
          tvp->tp[i].text);
  }
}

void MyClient::printBLOBVector(IBLOBVectorProperty *bvp)
{
  if (bvp == nullptr)
  {
    IDLog("%s: [ERROR] device %s prop %s not found\n",
          __func__, arguments.device_name, bvp->name);
    return;
  }

  IDLog("%s: %s Current values:\n", __func__, bvp->name);
  for (int i = bvp->nbp; i--;)
    IDLog("    %s: size %d bytes\n", bvp->bp[i].name, bvp->bp[i].bloblen);
}

void MyClient::newDevice(INDI::BaseDevice *dp)
{
  // Check if we have got the target device
  if (strcmp(dp->getDeviceName(), arguments.device_name) != 0) return;

  // Got the device
  ccd_simulator = dp;
  if (arguments.verbose)
    IDLog("%s: Received device: %s\n", __func__, dp->getDeviceName());
}

void MyClient::newProperty(INDI::Property *property)
{
  const char *property_name = property->getName();
  const char *device_name = property->getDeviceName();

  if (arguments.verbose) IDLog("%s: got '%s'\n", __func__, property_name);
  getInfo(property);

  if ( strcmp(device_name, arguments.device_name)!=0 ) return;
  if ( strcmp(property_name, "CONNECTION")==0 )
  {
    connectDevice(arguments.device_name);
    return;
  }
}

void MyClient::newNumber(INumberVectorProperty *nvp)
{
  // Check exposure time
  if( strcmp(nvp->name, "CCD_EXPOSURE")==0 )
    {
      for (uint8_t i = nvp->nnp; i--;)
        {
          if( strcmp(nvp->np[i].name, "CCD_EXPOSURE_VALUE")==0 )
            { IDLog("%s, %7.3f\n", nvp->np[i].name, nvp->np[i].value); }
        }
    }
}

void MyClient::newMessage(INDI::BaseDevice *dp, int messageID)
{
  if( strcmp(dp->getDeviceName(), arguments.device_name )!=0 ) return;
  IDLog( "Recveing message from Server:\n\n########################\n%s"
         "\n########################\n\n",
         dp->messageQueue(messageID).c_str() );
}

void MyClient::newBLOB(IBLOB *bp)
{
  std::ofstream myfile;

  // Save FITS file to disk
  myfile.open(arguments.output_name, std::ios::out | std::ios::binary);
  myfile.write(static_cast<char *>(bp->blob), bp->bloblen);
  myfile.close();
  IDLog("%s: received image, saved as '%s'\n", __func__,
        arguments.output_name);

  // Send finish signal
  {
    std::lock_guard<std::mutex> lk{mtx};
    processed = true;
    cv.notify_one();
  }
}
